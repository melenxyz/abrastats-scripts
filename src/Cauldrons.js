import fetch from "node-fetch";
import { createClient } from "redis";
import config from "../config/config.js";
import { getCauldronInfos } from "./cauldron.js";

let redis = null;

(async () => {
  redis = createClient();

  redis.on("error", (err) => console.log("Redis Client Error", err));

  await redis.connect();
})();

export const fetchCauldrons = async () => {
  let cauldrons = [];
  const response = await fetch(`${config.abracadabraApiUrl}/pools`, {
    method: "GET",
  });
  const jsonResponse = await response.json();
  cauldrons = jsonResponse.pools
    .sort((a, b) => a.name.localeCompare(b.name))
    .sort((a, b) => a.network - b.network);
  cauldrons.forEach((cauldrons) => {
    cauldrons.deprecated = cauldrons.name.includes("dep-");
  });
  return cauldrons;
};

export const fillCauldronsInfos = async (cauldronsRaw) => {
  let cauldrons = [];
  await Promise.all(
    cauldronsRaw.map(async (cauldron) => {
      let filledCauldron = {
        ...cauldron,
        idDeprecated: cauldron.name.includes("dep-"),
      };
      const cauldronInfos = await getCauldronInfos(cauldron);
      filledCauldron = {
        ...filledCauldron,
        ...cauldronInfos,
      };
      cauldrons.push({ ...filledCauldron });
    })
  );
  return cauldrons;
};

export const fetchAndFillCauldrons = new Promise(async (resolve, reject) => {
  console.log("Fetching and filling cauldrons ....");
  const cauldronsRaw = await fetchCauldrons();
  const cauldrons = await fillCauldronsInfos(cauldronsRaw);
  console.log("Cauldrons fetched !");
  resolve({ cauldrons });
});
