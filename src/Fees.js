import moment from "moment";
import fetch from "node-fetch";
import Web3 from "web3";
import config from "../config/config.js";

export const fetchDailyFees = new Promise(async (resolve, reject) => {
  console.log("Fetching daily fees ....");
  let dailyFees = [];
  let cumulativeFees = 0.0;
  const fromDate = moment("2021-06-01").toISOString();
  const days = moment().diff(moment(fromDate), "d");
  await Promise.all(
    [...Array(days).keys()].map(async (i) => {
      const from = moment(fromDate).add(i, "d").toISOString();
      const to = moment(fromDate)
        .add(i + 1, "d")
        .toISOString();

      const responseFees = await fetch(
        `${config.abracadabraApiUrl}/statistic/fees-earned`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            date: {
              from,
              to,
            },
          }),
        }
      );
      const jsonResponseFees = await responseFees.json();
      cumulativeFees += parseFloat(
        Web3.utils.fromWei(jsonResponseFees.totalFeesEarned)
      );
      dailyFees.push({
        timestamp: moment(from).add(i, "d").valueOf(),
        fees: parseFloat(Web3.utils.fromWei(jsonResponseFees.totalFeesEarned)),
        cumulativeFees: parseFloat(cumulativeFees).toFixed(0),
        cauldrons: jsonResponseFees.pools,
      });
    })
  );
  console.log("Daily fees fetched !", dailyFees);
  resolve({ dailyFees });
});
