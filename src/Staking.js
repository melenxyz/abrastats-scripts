import moment from "moment";
import fetch from "node-fetch";
import pkg from "@apollo/client/core/core.cjs";
import { createHttpLink } from "apollo-link-http";
import { StakedSpellRatioQuery } from "../datas/queries/StakedSpellRatioQuery.js";
import { StakedSpellAbracadabraGraph } from "../datas/constants/GraphURLs.js";

const { ApolloClient, InMemoryCache, gql } = pkg;

const apolloClient = new ApolloClient({
  link: createHttpLink({
    uri: StakedSpellAbracadabraGraph,
    fetch: fetch,
  }),
  cache: new InMemoryCache(),
});

export const fetchStakingRatio = new Promise(async (resolve, reject) => {
  console.log("Fetching staking ratio ....");
  const stakingRatioResponse = await apolloClient.query({
    query: gql(StakedSpellRatioQuery),
  });
  const from = moment(1622401177000);
  const days = moment().diff(moment(from), "days");

  let stakingRatio = [];
  for (let i = 0; i < days; i++) {
    const result = stakingRatioResponse.data.ratioUpdates.find((item) =>
      moment(parseInt(item.timestamp) * 1000).isSame(
        moment(from).add(i, "day"),
        "d"
      )
    );
    if (result) {
      stakingRatio.push({
        timestamp: moment(from).add(i, "day").unix(),
        ratio: parseFloat(result.ratio),
      });
    } else {
      stakingRatio.push({
        timestamp: moment(from).add(i, "day").unix(),
        ratio: i === 0 ? 1.0 : stakingRatio[stakingRatio.length - 1].ratio,
      });
    }
  }
  console.log("Staking ratio fetched !");
  resolve({ stakingRatio });
});
