import Web3 from "web3";
import { createRequire } from "module";
import { createClient } from "redis";
import { TREASURY_COMPOSITION } from "../datas/constants/Treasury.js";
import { TREASURY_ADDRESS } from "../datas/constants/Addresses.js";
import { getPrice } from "../helpers/coingecko.js";
import {
  getOraclePriceOfCauldron,
  getValueForDecimals,
  getValueObject,
  getWeb3InstanceForChain,
} from "../helpers/utils.js";

let redis = null;

(async () => {
  redis = createClient();

  redis.on("error", (err) => console.log("Redis Client Error", err));

  await redis.connect();
})();

const require = createRequire(import.meta.url);

const ERC20ABI = require("../datas/abi/ERC20ABI.json");
const VeCrvABI = require("../datas/abi/VeCrvABI.json");
const YearnVaultABI = require("../datas/abi/YearnVaultABI.json");
const CauldronABI = require("../datas/abi/CauldronABI.json");
const OracleABI = require("../datas/abi/OracleABI.json");
const StargateLpABI = require("../datas/abi/StargateLpABI.json");

const web3 = getWeb3InstanceForChain(1);

const getVeCRVInfos = async (address) => {
  const VeCrvContract = new web3.eth.Contract(VeCrvABI, address);
  const lockedVeCRV = await VeCrvContract.methods
    .locked(TREASURY_ADDRESS)
    .call();
  const votingPowerVeCRV = await VeCrvContract.methods
    .locked(TREASURY_ADDRESS)
    .call();
  return {
    amount: `${parseFloat(web3.utils.fromWei(lockedVeCRV.amount)).toFixed(3)}`,
    votingPowerVeCRV: `${parseFloat(
      web3.utils.fromWei(votingPowerVeCRV.amount)
    ).toFixed(3)}`,
  };
};

const getYearnVaultInfos = async (address, underlying) => {
  let { amount, pricePerShare } = 0;
  const YearnVaultContract = new web3.eth.Contract(YearnVaultABI, address);
  const tokenAmount = await YearnVaultContract.methods
    .balanceOf(TREASURY_ADDRESS)
    .call();
  const pricePerShareResult = await YearnVaultContract.methods
    .pricePerShare()
    .call();

  // needed because of the decimals
  if (underlying.name === "USDC" || underlying.name === "USDT") {
    amount = parseFloat(tokenAmount) / 1000000;
    pricePerShare = parseFloat(pricePerShareResult) / 1000000;
  } else {
    amount = web3.utils.fromWei(tokenAmount);
    pricePerShare = web3.utils.fromWei(pricePerShareResult);
  }

  return {
    amount: `${parseFloat(amount).toFixed(5)}`,
    pricePerShare: `${pricePerShare}`,
  };
};

export const fetchTreasury = new Promise(async (resolve, reject) => {
  console.log("Fetching treasury ....");
  let treasuryValue = 0.0;
  let treasuryAssets = [];
  let votingPower = 0.0;
  let totalLocked = 0.0;
  let veCrvValueObject = null;
  await Promise.all(
    TREASURY_COMPOSITION.map(async (treasuryAsset) => {
      const { name, type, address, underlying, coingeckoId } = treasuryAsset;
      let tokenInfos = {
        amount: 0,
      };
      if (type === "ve-crv") {
        const { amount, votingPowerVeCRV } = await getVeCRVInfos(address);
        const tokenPrice = await getPrice(coingeckoId);
        votingPower = votingPowerVeCRV;
        const valueObject = await getValueObject(tokenPrice, amount);
        veCrvValueObject = valueObject;
        tokenInfos = {
          amount,
          price: `${tokenPrice}`,
          value: valueObject,
        };
      }

      if (type === "yearn-vault") {
        const { amount, pricePerShare } = await getYearnVaultInfos(
          address,
          underlying
        );

        const oraclePrice = await getOraclePriceOfCauldron(underlying.cauldron);
        const tokenPrice =
          (1 / oraclePrice) *
          Math.pow(
            10,
            underlying.name === "USDT" || underlying.name === "USDC" ? 6 : 18
          );

        const valueObject = await getValueObject(
          tokenPrice,
          parseFloat(amount) * (1 / parseFloat(pricePerShare))
        );

        tokenInfos = {
          amount,
          price: `${tokenPrice}`,
          value: valueObject,
        };
      }
      if (type == "token") {
        const AssetContract = new web3.eth.Contract(ERC20ABI, address);
        const balance = await AssetContract.methods
          .balanceOf(TREASURY_ADDRESS)
          .call();

        const tokenPrice = await getPrice(coingeckoId);

        let valueObject = await getValueObject(
          tokenPrice,
          web3.utils.fromWei(balance)
        );
        let amount = parseFloat(web3.utils.fromWei(balance)).toFixed(5);
        if (name === "USDT") {
          valueObject = await getValueObject(tokenPrice, balance / 1000000);
          amount = `${parseFloat(balance / 1000000).toFixed(3)}`;
        }
        tokenInfos = {
          price: `${tokenPrice}`,
          value: valueObject,
          amount,
        };
      }
      if (type == "curve-lp") {
        const AssetContract = new web3.eth.Contract(ERC20ABI, address);
        const balance = await AssetContract.methods
          .balanceOf(TREASURY_ADDRESS)
          .call();
        const tokenPrice = await getPrice(coingeckoId);
        const valueObject = await getValueObject(
          tokenPrice,
          web3.utils.fromWei(balance)
        );

        tokenInfos = {
          amount: parseFloat(web3.utils.fromWei(balance)).toFixed(3),
          price: `${tokenPrice}`,
          value: valueObject,
        };
      }
      if (type === "stargate-lp") {
        const StargateContract = new web3.eth.Contract(StargateLpABI, address);
        const balanceRaw = await StargateContract.methods
          .balanceOf(TREASURY_ADDRESS)
          .call();
        const decimals = await StargateContract.methods.decimals().call();
        const balance = getValueForDecimals(balanceRaw, decimals);

        const totalLiquidity = await StargateContract.methods
          .totalLiquidity()
          .call();

        const totalSupply = await StargateContract.methods
          .totalLiquidity()
          .call();

        const underlyingAmount =
          parseFloat(balance) *
          parseFloat(parseFloat(totalSupply) / parseFloat(totalLiquidity));

        const oraclePrice = await getOraclePriceOfCauldron(underlying.cauldron);
        const tokenPrice = getValueForDecimals(oraclePrice, decimals);
        const valueObject = await getValueObject(tokenPrice, underlyingAmount);
        tokenInfos = {
          amount: parseFloat(underlyingAmount).toFixed(3),
          price: `${tokenPrice}`,
          value: valueObject,
        };
      }
      if (parseFloat(tokenInfos.value.usd) !== 0.0) {
        if (type !== "ve-crv") {
          treasuryValue = treasuryValue + parseFloat(tokenInfos.value.usd);
        }
        treasuryAssets.push({
          ...treasuryAsset,
          ...tokenInfos,
        });
      }
    })
  );
  // for (let i = 0; i < TREASURY_COMPOSITION.length; i++) {

  // }

  const treasuryValueObject = await getValueObject(1, treasuryValue);

  // const ethPrice = await getPrice("ethereum");
  // const treasuryValueEth = parseFloat(treasuryValue.toFixed(0) / ethPrice);
  // const btcPrice = await getPrice("bitcoin");
  // const treasuryValueBtc = parseFloat(treasuryValue.toFixed(0) / btcPrice);
  console.log("Treasury fetched !");
  resolve({
    treasury: {
      address: TREASURY_ADDRESS,
      value: treasuryValueObject,
      veCrv: {
        value: veCrvValueObject,
        votingPower,
        totalLocked,
      },
      assets: treasuryAssets.sort(
        (a, b) => parseFloat(b.value.usd) - parseFloat(a.value.usd)
      ),
    },
  });
});
