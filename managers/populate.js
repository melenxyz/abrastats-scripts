import { createClient } from "redis";
import { fetchAndFillCauldrons } from "../src/Cauldrons.js";
import { fetchDailyFees } from "../src/Fees.js";
import { fetchStakingRatio } from "../src/Staking.js";
import { fetchTreasury } from "../src/Treasury.js";

let redis = null;

(async () => {
  redis = createClient();

  redis.on("error", (err) => console.log("Redis Client Error", err));

  await redis.connect();
})();

const populate = async () => {
  return Promise.all([
    fetchStakingRatio,
    fetchAndFillCauldrons,
    fetchTreasury,
    fetchDailyFees,
  ]).then(async (values) => {
    await redis.set(
      "abrastats.stakingRatio",
      JSON.stringify(values[0].stakingRatio)
    );
    await redis.set("abrastats.cauldrons", JSON.stringify(values[1].cauldrons));
    await redis.set("abrastats.treasury", JSON.stringify(values[2].treasury));
    await redis.set("abrastats.dailyFees", JSON.stringify(values[3].dailyFees));
  });
};

await populate();
process.exit(0);
