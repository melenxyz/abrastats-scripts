const RPCs = {
  ETH_RPC_URL: "https://rpc.flashbots.net",
  AVALANCHE_RPC_URL: "https://api.avax.network/ext/bc/C/rpc",
  FANTOM_RPC_URL: "https://rpc.ftm.tools",
  ARBITRUM_RPC_URL: "https://arb1.arbitrum.io/rpc",
  BINANCE_RPC_URL: "https://bsc-dataseed1.binance.org:443",
};

export default RPCs;
