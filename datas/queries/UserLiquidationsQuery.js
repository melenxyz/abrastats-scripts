export const UserLiquidationsQuery = `
query 
GetUserLiquidations($address: String!) { 
    userLiquidations(where: {user : $address, timestamp_gt: 0}) {
        transaction 
        exchangeRate 
        timestamp 
        loanRepaid 
        collateralRemoved 
        cauldron 
        { collateralSymbol } 
    }
}`;
